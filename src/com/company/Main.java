package com.company;

public class Main {
    // всегда устанавливайте самый строгий уровень доступа из возможных
    private static Person[] getPersonArray() {
        int n = 10;
        Person[] person = new Person[n];
        for (int i = 0; i < n; i++) {
            person[i] = new Person();
        }
        return person;
    }

    private static void arrPrint(Person[] p) {
        // выводим массив
        for(Person x : p) {
            // формированию строки, представляющей данные класса место в toString
            System.out.println(x);
        }
        System.out.println();
    }

    private static void personArrayWithSort(SortParent sort) {
        System.out.println("Unsorted Array");
        Person[] p = getPersonArray(); // формируем массив случайных элементов
        arrPrint(p);// выводим массив

        sort.runSortWithTimer(p); // сортируем
        arrPrint(p);// выводим массив
    }

    public static void main(String[] args) {
        // подумайте как можно избежать повторения кода с помощью полиморфизма

        // SORT 1
        BubbleSort s1 = new BubbleSort(); // создаем объект для первой сортировки
        personArrayWithSort(s1);

        // SORT 2
        SelectionSort s2 = new SelectionSort();// создаем объект для второй сортировки
        personArrayWithSort(s2);
    }
}
