package com.company;

/**
 * My custom exception class.
 */
class MyException extends Exception
{
    public MyException(String message)
    {
        super(message);
    }
}
