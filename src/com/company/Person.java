package com.company;

import java.util.Random;

public class Person {
    // используйте самый строгий уровень доступа. Для полей вообще почти всегда ставят private
    // [Artem] добавил private и гет-методы
    private int age;
    private Sex sex;
    private String name;

    @Override
    public String toString() {
        return "name = " + name + ", age = " + age + ", sex = " + sex;
    }

    public Person() {
        //переменную лучше объявлять блише всего к использованию и максимально сужать область видимости, в данном случае это конструктор
        final Random random = new Random();

        this.age = random.nextInt(101); // fill age

        // fill sex
        // можно проще с помощью тернарного оператора и nextBoolean
        this.sex = (random.nextBoolean() == true) ? Sex.MAN : Sex.WOMAN;
        this.name = randomName(5);// fill name
    }

    public int getAge() {
        return age;
    }

    public Sex getSex() {
        return sex;
    }

    public String getName() {
        return name;
    }

    // генератор случайных имен
    private String randomName(int len) {
        final Random random = new Random();
        StringBuilder builder = new StringBuilder();
        String name = "";
        for (int i = 0; i < len; i++) {
            char c = (char) (random.nextInt(26) + 97); // генерируем случайное число
            // не критично, но чтобы не плодить много строк лучше использовать StringBuilder
            builder = builder.append(c);
        }
        return builder.toString();
    }
}



