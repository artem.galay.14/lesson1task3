package com.company;

// sort 2 (Selection Sort)
public class SelectionSort extends SortParent {
    @Override
    void sort(Person[] p) throws MyException {
        System.out.println("SelectionSort");
        for (int left = 0; left < p.length; left++) {
            int minInd = left;
            for (int i = left+1; i < p.length; i++) {
                if (comparePerson(p[i], p[minInd]) == -1) {
                    minInd = i;
                }
            }
            swap(p, left, minInd);
        }
    }
}
