package com.company;

// всегда выбирайте понятные названия для классов. Sort1 ни о чём не говорит
// sort 1 (Bubble Sort)
public class BubbleSort extends SortParent {
    @Override
    void sort(Person[] p) throws MyException {
        System.out.println("BubbleSort");
        boolean needIteration = true;
        while (needIteration) {
            needIteration = false;
            for (int i = 1; i < p.length; i++) {
                if (comparePerson(p[i-1], p[i]) == 1) {
                    swap(p, i, i-1);
                    needIteration = true;
                }
            }
        }
    }
}
