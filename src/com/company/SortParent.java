package com.company;

public abstract class SortParent {


    //compare two Persons for sort
    public int comparePerson(Person p1, Person p2) throws MyException {
        // не используйте == при сравнении срок, equals - наше всё
        if (p1.getName().equals(p2.getName()) & p1.getAge() == p2.getAge()) {
            throw new MyException("имена людей и возраст совпадают");
        }

        if (p1.getSex() == Sex.MAN & p2.getSex() == Sex.WOMAN) {
            return -1;
        } else if (p1.getSex() == Sex.WOMAN & p2.getSex() == Sex.MAN) {
            return 1;
        } else {
            if (p1.getAge() < p2.getAge()) {
                return 1;
            } else if (p1.getAge() > p2.getAge()) {
                return -1;
            } else {
                if (p1.getName().compareTo(p2.getName()) >= 0) {
                    return 1;
                } else {
                    return -1;
                }
            }
        }
    }

    // swap two elements of array
    public void swap(Person[] array, int ind1, int ind2) {
        Person tmp = array[ind1];
        array[ind1] = array[ind2];
        array[ind2] = tmp;
    }

    // sort 1 (Bubble Sort)
    abstract void sort(Person[] p) throws MyException;

    // понимаю, что вы хотели разделить логику замера времени и самой сортировки. Это хорошо, но всё же названия doSort и sort ни о чём не говорят
    // разница не ясна
    public void runSortWithTimer(Person[] p) {
        long t1, t2;
        t1 = System.nanoTime();
        try {
            this.sort(p); //запускаем сортировку
        } catch (MyException e) {
            System.out.println(e.getMessage());
        }
        t2 = System.nanoTime();
        System.out.println("Nanoseconds: " + (t2-t1));
    }
}
